'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('opdconsultancies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      date: {
        type: Sequelize.DATEONLY
      },
      bdlevel: {
        type: Sequelize.STRING
      },
      fever: {
        type: Sequelize.STRING
      },
      prescribeddrugs: {
        type: Sequelize.STRING
      },
      patientId: {
        type: Sequelize.INTEGER
      },
      patientcnic: {
        type: Sequelize.STRING
      },
      comments: {
        type: Sequelize.STRING
      },
      potentialdisease: {
        type: Sequelize.STRING
      },
      otherproblem: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('opdconsultancies');
  }
};