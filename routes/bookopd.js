var express = require('express');
var router = express.Router();
var db = require('../models/index');
var verify = require('../middleware');


router.post('/', verify.rou, function (req, res, next) {
    var lastbookingid;
    let opdbook = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        cnicno: req.body.cnicno,
        contactno: req.body.contactno,
        time: req.body.time,
        date: req.body.date,
        opdId: req.body.opdId
    }
    db.opdData.findOne({
        where: {
            id: opdbook.opdId
        }
    }).then(function (opd) {
        if (!opd) {
            res.statusCode = 404;
            var resBody = {
                // error: err.errors,
                suucess: false,
                message: "Entered OPD ID is not Valid",
            }
            res.send(resBody);
        }
        else if (opdbook.time != opd.timingofODs) {
            res.statusCode = 400;
            var resBody = {
                // error: err.errors,
                suucess: false,
                message: "Entered Time and Opd Timing is not matching",
            }
            res.send(resBody);
        }
        else {
            db.opdbooking.findOne({
                where: {
                    opdId: opdbook.opdId,
                    date: opdbook.date
                }
            }).then(function (booking) {
                if (!booking) {
                    db.opdbooking.create(opdbook).then(function (response) {
                        response.bookingId = 1;
                        response.save();
                        res.send(response);
                    })
                }
                else {
                    db.opdbooking.max('bookingId',
                        {
                            where:
                            {
                                opdId:
                                { like: opdbook.opdId },
                                date: {
                                    like: opdbook.date
                                }
                            }
                        }).then(max => {
                            lastbookingid = max
                        })
                    if (lastbookingid > 25) {
                        res.statusCode = 400;
                        var resBody = {
                            // error: err.errors,
                            suucess: false,
                            message: "Can't take more bookings as reached limit for today",
                        }
                        res.send(resBody);
                    }
                    else {
                        db.opdbooking.create(opdbook).then(function (response) {
                            response.bookingId = lastbookingid + 1;
                            response.save();
                            res.send(response);
                        })
                    }
                }
            },
                function (err) {
                    res.statusCode = 500;
                    var resBody = {
                        error: err.errors,
                        suucess: false,
                        message: err.message,
                    }
                    res.send(resBody);
                })
        }
    },
        function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});

router.delete('/del/:id', verify.rou, function (req, res, next) {
    db.opdbooking.destroy({
        where: {
            id: req.params.id
        }
    })
        .then(
        function (response) {
            if (response == 0) {
                res.statusCode = 404;
                var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "Entered Id is not correct. Enter Correct Id",
                }
                res.send(resBody);
            }
            else {
                res.send(String(response));
            }
        },
        function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});

router.get('/getdetailsbyopdid/:id', verify.rou, function (req, res, next) {
    db.opdbooking.findAll({
        where: {
            opdId: req.params.id
        }
    }).then(
        function (response) {
            if (!response) {
                res.statusCode = 404;
                var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "Entered OPD Id is not correct. Enter Correct Id",
                }
                res.send(resBody);
            }
            else {
                res.send(response);
            }
        }, function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});

router.put('/updaterecord/:id', verify.rou, function (req, res, next) {
    //is k edit main date and time ki field hata dena
    var oid, lastbookingid
    db.opdbooking.findAll({
        where: {
            id: req.params.id
        }
    })
        .then(
        function (ob) {
            if (ob == null) {
                res.statusCode = 404;
                var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "Entered ID is not Valid",
                }
                res.send(resBody);
            }
            else {
                oid = ob.opdId;
                ob.update(req.body,
                    {
                        where: {
                            id: req.params.id
                        }
                    })
                if (oid == ob.opdId) {
                    res.send(ob);
                }
                else {
                    db.opdData.findOne({
                        where: {
                            id: ob.opdId
                        }
                    }).then(function (opd) {
                        if (!opd) {
                            res.statusCode = 400;
                            var resBody = {
                                // error: err.errors,
                                suucess: false,
                                message: "OPD ID set back to old OPD ID as entered ID was not valid",
                            }
                            ob.opdId = oid;
                            ob.save();
                            res.send(resBody);
                        }
                        else {
                            db.opdbooking.findOne({
                                where: {
                                    opdId: ob.id
                                }
                            }).then(function (check) {
                                if (!check) {
                                    ob.bookingId = 1;
                                    ob.save();
                                    res.send(ob);
                                }
                                else {
                                    db.opdbooking.max('bookingId', {
                                        where:
                                        {
                                            opdId:
                                            { like: ob.opdId }
                                        }
                                    }).then(max => {
                                        lastbookingid = max
                                    })
                                    ob.bookingId = lastbookingid + 1
                                    ob.save();
                                    res.send(ob);
                                }
                            })
                        }
                    })
                }
                // res.send(ob);
            }
        },
        function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});


router.get('/byid/:id', verify.rou, function (req, res, next) {

    db.opdbooking.findOne({
        where: {
            id: req.params.id
        }
    }).then(
        function (response) {
            if (response == null) {
                res.statusCode = 404;
                var resBody = {
                    suucess: false,
                    message: "Wrong ID inputed",
                }
                res.send(resBody);
            }
            else {
                res.send(response);
            }
        },
        function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});

module.exports = router;