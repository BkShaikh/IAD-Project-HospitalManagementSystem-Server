var express = require('express');
var router = express.Router();
var db = require('../models/index');
var verify = require('../middleware');

router.post('/', verify.rou, function (req, res, next) {
    let consultancy = {
        date: req.body.date,
        bdlevel: req.body.bdlevel,
        fever: req.body.fever,
        prescribeddrugs: req.body.prescribeddrugs,
        patientId: req.body.patientId,
        patientcnic: req.body.patientcnic,
        comments: req.body.comments,
        potentialdisease: req.body.potentialdisease,
        otherproblem: req.body.otherproblem
    }
    db.opdbooking.findOne({             //checking in opdbooking that whether adding data for patient has booking or not ? 
        where: {
            cnicno: consultancy.patientcnic
        }
    }).then(
        function (patient) {
            if (!patient) {
                res.statusCode = 400;
                var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "There is no such patient booking available",
                }
                res.send(resBody);
            }
            else {
                db.opdconsultancy.create(consultancy).
                    then(
                    function (response) {
                        res.send(response)
                    })
            }
        })
});
router.put('/updaterecord/:id', verify.rou, function (req, res, next) {
    //is k edit main patietn id dalne ki field nhi hogi
    db.opdconsultancy.findOne({
        where: {
            id: req.params.id
        }
    })
        .then(
        function (consultancy) {
            if (consultancy == null) {
                res.statusCode = 404;
                var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "Entered ID is not Valid",
                }
                res.send(resBody);
            }
            else {
                consultancy.update(req.body,
                    {
                        where: {
                            id: req.params.id
                        }
                    });
                res.send(consultancy);
            }
        }, function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});

router.delete('/del/:id', verify.rou, function (req, res, next) {

    db.opdconsultancy.destroy({
        where: {
            id: req.params.id
        }
    })
        .then(
        function (response) {
            if (response == 0) {
                res.statusCode = 404;
                var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "Entered Id is not correct. Enter Correct Id",
                }
                res.send(resBody);
            }
            else {
                res.send(String(response));
            }
        },
        function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});

router.get('/getbycnic/:cnic', verify.rou, function (req, res, next) {
    var cnicnum = req.params.cnic
    db.opdbooking.findOne({
        where: {
            cnicno: cnicnum
        }
    }).then(
        function (response) {
            if (!response) {
                res.statusCode = 400;
                var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "Entered CNIC is not correct. Enter Correct CNIC and then search",
                }
                res.send(resBody);
            }
            else {
                db.opdconsultancy.findAll({
                    where: {
                        patientcnic: cnicnum
                    }
                }).then(function (response) {
                    res.send(response);
                })
            }
        }, function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});
router.get('/byid/:id', verify.rou, function (req, res, next) {

    db.opdbooking.findOne({
        where: {
            id: req.params.id
        }
    }).then(
        function (response) {
            if (response == null) {
                res.statusCode = 404;
                var resBody = {
                    suucess: false,
                    message: "Wrong ID inputed",
                }
                res.send(resBody);
            }
            else {
                res.send(response);
            }
        },
        function (err) {
            res.statusCode = 500;
            var resBody = {
                error: err.errors,
                suucess: false,
                message: err.message,
            }
            res.send(resBody);
        })
});
module.exports = router;