var express = require('express');
var router = express.Router();
var db = require('../models/index');
var verify = require('../middleware');

router.get('/', verify.rou, function (req, res, next) {

  db.roomData.findAll({}).then(
    function (response) {
      res.send(response);
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});

router.get('/byid/:id', verify.rou, function (req, res, next) {

  db.roomData.findOne({
    where: {
      id: req.params.id
    }
  }).then(
    function (response) {
      if (!response) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered ID is not Valid",
        }
        res.send(resBody);
      }
      else {
        res.send(response);
      }

    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message + " " + "Following is not a vaild ID",
      }
      res.send(resBody);
    })


});

router.post('/', verify.rou, function (req, res, next) {
  let room = {
    floorNo: req.body.floorNo,
    roomAssignedFor: req.body.roomAssignedFor,
    roomNo: req.body.roomNo,
    assignedTo: req.body.assignedTo
  };
  console.log(req.body);
  if (room.floorNo > 5) {
    res.statusCode = 400;
    var resBody = {
      // error: err.errors,
      suucess: false,
      message: "Can't Assign this floorNo., As there is no such floor",
    }
    res.send(resBody);
  }
  else {
    db.roomData.create(room).then(
      function (response) {
        res.send(response);
      },
      function (err) {


        res.statusCode = 500;
        var resBody = {
          error: err.errors,
          suucess: false,
          message: err.message,
        }
        res.send(resBody);
      })
  }

});


router.delete('/del/:id', verify.rou, function (req, res, next) {
  db.roomData.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (response) {
      console.log(response);
      if (response == 0) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered id is not correct enter correct id",
        }
        res.send(resBody);
      }
      else {
        res.send(String(response));
      }
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});


router.patch('/update/:id', verify.rou, function (req, res, next) {
  const updates = req.body.updates;
  db.roomData.findOne({
    where: {
      id: req.params.id
    }
  })

    .then(room => {
      return room.updateAttributes(updates)
    })
    // .then(function (response) {
    //   if (response == null) {
    //     res.statusCode = 404;
    //     var resBody = {
    //       suucess: false,
    //     }
    //     res.send(resBody);
    //   }
    // })
    .then(updatedRoom => {
      if (updatedRoom == null) {
        console.log("idher aya")
        res.statusCode = 404;
        var resBody = {
          suucess: false,
        }
        res.send(resBody);
      }
      else
        res.send(updatedRoom);
    },
    function (err) {

      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message + " " + "maybe you're entering unvalid Room ID ",
      }
      res.send(resBody);
    }
    );

});

router.put('/updaterecord/:id', verify.rou, function (req, res, next) {

  // db.doctor.findOne({          //pagal kya bhnd mara hai lanat hai aap pr :/
  //   where: {
  //     id: req.params.id
  //   }
  // })
  db.roomData.findOne({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (room) {
      if (room == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered ID is not Valid",

        }
        res.send(resBody);
      }
      else {
        room.update(req.body,
          {
            where: {
              id: req.params.id
            }
          });
        res.send(room);
      }
    }, function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )

});


// router.get('/details/:id',verify.rou , function (req, res, next){
// // yahan se dr ki details ya patient ki deatils la sukte hain 
// });

router.get('/getdoctordetail/:id', verify.rou, function (req, res, next) {

  //dr. jo is room ka hai us ki details ajaen gi
  db.roomData.find({
    where: { id: req.params.id }

  })
    .then(

    function (room) {
      db.doctordata.findOne({ where: { roomId: room.id } }).then(function (data) {
        if (data == null) {
          res.statusCode = 400;
          var resBody = {
            // error: err.errors,
            suucess: false,
            message: "There is no data registered against this Id",

          }
          res.send(resBody);
        }
        else {
          res.send(data);
        }
      })

    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    })
  // console.log("abcdefg")

});


router.get('/getpatientdetail/:id', verify.rou, function (req, res, next) {

  //patient jo is room ka hai us ki details ajaen gi
  db.roomData.find({
    where: { id: req.params.id }

  })
    .then(

    function (room) {
      db.patientdata.findOne({ where: { roomId: room.id } }).then(function (data) {
        if (data == null) {
          res.statusCode = 400;
          var resBody = {
            // error: err.errors,
            suucess: false,
            message: "There is no data registered against this Id",

          }
          res.send(resBody);
        }
        else {
          res.send(data);
        }
      })

    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    })
  // console.log("abcdefg")

});

router.post('/patientremovefromroombed/:id/:id1', verify.rou, function (req, res, next) {
  // to make roomId attribute null in specific patient table
  const rid = req.params.id;
  const pid = req.params.id1;

  db.roomData.findOne({
    where: {
      id: rid
    }
  })
    .then(
    function (room) {
      // console.log(rid);
      // console.log(pid);
      if (room == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of room is not correct"
        }
        res.send(resBody);
      }
      else {
        db.patientdata.findOne({ where: { id: pid } })
          .then(
          function (patient) {
            if (patient == null) {
              res.statusCode = 404;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "Enterd ID of Patient is not correct"
              }
              res.send(resBody);
            }
            else {
              // console.log(doctor.roomId)
              patient.roomId = null;
              patient.save();
              console.log(patient.roomId)
              res.send(patient);
            }
          })
      }
    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )

});


router.post('/doctorremovefromroom/:id/:id1', verify.rou, function (req, res, next) {
  // to make roomId attribute null in specific doctor table
  const rid = req.params.id;
  const did = req.params.id1;

  db.roomData.findOne({
    where: {
      id: rid
    }
  })
    .then(
    function (room) {
      // console.log(rid);
      // console.log(pid);
      if (room == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of room is not correct"
        }
        res.send(resBody);
      }
      else {
        db.doctordata.findOne({ where: { id: did } })
          .then(
          function (doctor) {
            // console.log(doctor.roomId)
            if (doctor == null) {
              res.statusCode = 404;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "Enterd ID of Doctor is not correct"
              }
              res.send(resBody);
            }
            else {
              doctor.roomId = null;
              doctor.save();
              console.log(doctor.roomId)
              res.send(doctor);
            }
          })
      }
    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )

});


router.get('/getopddetail/:id', verify.rou, function (req, res, next) {

  //sare patients jo jo is ward ka hai us ki details ajaen gi
  db.roomData.find({
    where: { id: req.params.id }

  })
    .then(

    function (room) {
      db.opdData.findOne({ where: { roomId: room.id } }).then(function (data) {
        if (data == null) {
          res.statusCode = 404;
          var resBody = {
            // error: err.errors,
            suucess: false,
            message: "There is no data registered against this Id",

          }
          res.send(resBody);
        }
        else {
          res.send(data);
        }
      })

    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    })
  // console.log("abcdefg")

});

router.post('/roomidtoopd/:id/:id1', verify.rou, function (req, res, next) {
  //first id of roomId to check weather room is available or not... 
  //second id of opd whom you want to give room to
  const rid = req.params.id;
  const oid = req.params.id1;

  db.roomData.findOne({
    where: {
      id: rid
    }
  }).then(
    function (room) {
      console.log(room);
      if (!room) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of Room is not correct"
        }
        res.send(resBody);
      }
      // else if(aaa==aaa){

      // }
      else {
        db.opdData.findOne({
          where: { roomId: rid }
        })
          .then(
          function (opd) {
            console.log("opd");
            if (opd == null) {
              db.opdData.findOne({
                where: { id: oid }
              }).then(
                function (op) {
                  if (!op) {
                    res.statusCode = 404;
                    var resBody = {
                      // error: err.errors,
                      suucess: false,
                      message: "Enterd ID of OPD is not correct"
                    }
                    console.log("bilal is pagal");
                    res.send(resBody);
                  }
                  else if (op.roomId != null) {
                    res.statusCode = 400;
                    var resBody = {
                      // error: err.errors,
                      suucess: false,
                      message: "This OPD has already been assigned a room. Empty opd current room first to give new room"
                    }
                    console.log("bilal is pagal part 2");
                    res.send(resBody);
                  }
                  else {
                    console.log("nhi hai room pehle se is k pas")
                    op.roomId = rid;
                    op.save();
                    res.send(op);
                  }
                })
            }
            else {
              res.statusCode = 400;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "This room is already occupied"
              }
              res.send(resBody);
            }

          })
      }
    })
});

router.post('/opdremovefromroom/:id/:id1', verify.rou, function (req, res, next) {
  // to make roomId attribute null in specific opd table
  const rid = req.params.id;
  const oid = req.params.id1;

  db.roomData.findOne({
    where: {
      id: rid
    }
  })
    .then(
    function (room) {
      // console.log(rid);
      // console.log(pid);
      if (room == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of room is not correct"
        }
        res.send(resBody);
      }
      else {
        db.opdData.findOne({ where: { id: oid } })
          .then(
          function (opd) {
            // console.log(doctor.roomId)
            if (opd == null) {
              res.statusCode = 404;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "Enterd ID of OPD is not correct"
              }
              res.send(resBody);
            }
            else {
              opd.roomId = null;
              opd.save();
              console.log(opd.roomId)
              res.send(opd);
            }
          })
      }
    }
    , function (err) {
      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});


router.post('/roomidtodoctor/:id/:id1', verify.rou, function (req, res, next) {
  //first id of roomId to check weather room is available or not... 
  //second id of doctor whom you want to give room to
  const rid = req.params.id;
  const did = req.params.id1;

  db.roomData.findOne({
    where: {
      id: rid
    }
  }).then(
    function (room) {
      console.log(room);
      if (!room) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of Room is not correct"
        }
        res.send(resBody);
      }
      // else if(aaa==aaa){

      // }
      else {
        db.doctordata.findOne({
          where: { roomId: rid }
        })
          .then(
          function (doctor) {
            console.log("doctor");
            if (doctor == null) {
              db.doctordata.findOne({
                where: { id: did }
              }).then(
                function (doc) {
                  if (!doc) {
                    res.statusCode = 404;
                    var resBody = {
                      // error: err.errors,
                      suucess: false,
                      message: "Enterd ID of doctor is not correct"
                    }
                    console.log("bilal is pagal");
                    res.send(resBody);
                  }
                  else if (doc.roomId != null) {
                    res.statusCode = 400;
                    var resBody = {
                      // error: err.errors,
                      suucess: false,
                      message: "This Doctor has Already a room empty his current room first to give him this room"
                    }
                    console.log("bilal is pagal part 2");
                    res.send(resBody);
                  }
                  else {
                    console.log("nhi hai room pehle se is k pas")
                    doc.roomId = rid;
                    doc.save();
                    res.send(doc);
                  }
                })
            }
            else {
              res.statusCode = 400;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "This room is already occupied"
              }
              res.send(resBody);
            }

          })
      }
    })
});


router.post('/roomidtopatient/:id/:id1', verify.rou, function (req, res, next) {
  //first id of roomId to check weather room is available or not... 
  //second id of patient whom you want to give room to
  const rid = req.params.id;
  const did = req.params.id1;

  db.roomData.findOne({
    where: {
      id: rid
    }
  }).then(function (room) {
    if (!room) {
      res.statusCode = 404;
      var resBody = {
        // error: err.errors,
        suucess: false,
        message: "Enterd ID of Room is not correct"
      }
      res.send(resBody);
    }
    else {
      db.patientdata.findOne({
        where: { roomId: rid }
      })
        .then(
        function (patient) {
          console.log("doctor");
          if (patient == null) {
            db.patientdata.findOne({
              where: { id: did }
            }).then(
              function (pati) {
                if (!pati) {
                  res.statusCode = 404;
                  var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "Enterd ID of Patient is not correct"
                  }
                  console.log("bilal is pagal");
                  res.send(resBody);
                }
                else if (pati.roomId != null) {
                  res.statusCode = 400;
                  var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "This Patient is already admit in the room number" + " " + room.roomNo
                  }
                  console.log("bilal is pagal part 2");
                  res.send(resBody);
                }
                else if (pati.wardId != null) {
                  res.statusCode = 400;
                  var resBody = {
                    // error: err.errors,
                    suucess: false,
                    message: "This Patient has already admitted in Ward number" + " " + pati.wardId
                  }
                  console.log("bilal is pagal part 2" + " ");
                  res.send(resBody);
                }
                else {
                  console.log("nhi hai room pehle se is k pas")
                  pati.roomId = rid;
                  pati.save();
                  res.send(pati);
                }
              })
          }
          else {
            res.statusCode = 400;
            var resBody = {
              // error: err.errors,
              suucess: false,
              message: "This room is already occupied"
            }
            res.send(resBody);
          }

        })
    }
  })
});



module.exports = router;