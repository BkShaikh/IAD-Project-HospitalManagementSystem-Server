var express = require('express');
var router = express.Router();
var db = require('../models/index');
var verify = require('../middleware');

router.get('/', verify.rou, function (req, res, next) {

  db.wardData.findAll({}).then(
    function (response) {
      res.send(response);
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});

router.get('/byid/:id', verify.rou, function (req, res, next) {

  db.wardData.findOne({
    where: {
      id: req.params.id
    }
  }).then(
    function (response) {
      res.send(response);
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message + " " + "Following is not a vaild ID",
      }
      res.send(resBody);
    })


});




router.post('/', verify.rou, function (req, res, next) {
  let ward = {
    floorNo: req.body.floorNo,
    totalNoOfBeds: req.body.totalNoOfBeds,
    noOfBedsEmpty: req.body.noOfBedsEmpty,
    wardcategory: req.body.wardcategory,
    wardsgender: req.body.wardsgender
  };
  // console.log(req.body);
  // console.log(ward.totalNoOfBeds + " " + ward.noOfBedsEmpty);
  // res.send("aaa");
  if (ward.floorNo > 5) {
    res.statusCode = 400;
    var resBody = {
      // error: err.errors,
      suucess: false,
      message: "Can't Assign this floorNo., As there is no such floor",
    }
    res.send(resBody);
  }
  else if (ward.noOfBedsEmpty != ward.totalNoOfBeds) {
    res.statusCode = 400;
    var resBody = {
      // error: err.errors,
      suucess: false,
      message: "Total Number Of beds should be equal to Total number of Empty Beds Initially",
    }
    res.send(resBody);
  }
  else {
    db.wardData.create(ward).then(
      function (response) {
        res.send(response);

      },
      function (err) {


        res.statusCode = 500;
        var resBody = {
          error: err.errors,
          suucess: false,
          message: err.message,
        }
        res.send(resBody);
      })
  }
  // db.wardData.create(ward).then(
  //   function (response) {
  //     console.log(response.totalNoOfBeds);
  //     if (response.noOfBedsEmpty != response.totalNoOfBeds) {
  //       res.statusCode = 400;
  //       var resBody = {
  //         // error: err.errors,
  //         suucess: false,
  //         message: "Total Number Of beds should be equal to Total number of Empty Beds Initially",
  //       }
  //       res.send(resBody);
  //     }
  //     else {
  //       res.send(response);
  //     }
  //   },
  //   function (err) {


  //     res.statusCode = 500;
  //     var resBody = {
  //       error: err.errors,
  //       suucess: false,
  //       message: err.message,
  //     }
  //     res.send(resBody);
  //   })
});
router.delete('/del/:id', verify.rou, function (req, res, next) {
  db.wardData.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (response) {
      if (response == 0) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered id is not correct enter correct id",
        }
        res.send(resBody);
      }
      else {
        res.send(String(response));
      }
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});


router.patch('/update/:id', verify.rou, function (req, res, next) {
  const updates = req.body.updates;
  db.wardData.findOne({
    where: {
      id: req.params.id
    }
  })

    .then(ward => {
      return ward.updateAttributes(updates)
    })
    // .then(function (response) {
    //   if (response == null) {
    //     res.statusCode = 404;
    //     var resBody = {
    //       suucess: false,
    //     }
    //     res.send(resBody);
    //   }
    // })
    .then(updatedWard => {
      if (updatedWard == null) {
        console.log("idher aya")
        res.statusCode = 404;
        var resBody = {
          suucess: false,
        }
        res.send(resBody);
      }
      else
        res.send(updatedWard);
    },
    function (err) {

      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message + " " + "maybe you're entering unvalid Ward ID ",
      }
      res.send(resBody);
    }
    );

});


router.put('/updaterecord/:id', verify.rou, function (req, res, next) {

  // db.doctor.findOne({          //pagal kya bhnd mara hai lanat hai aap pr :/
  //   where: {
  //     id: req.params.id
  //   }
  // })
  db.wardData.findOne({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (ward) {
      if (ward == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered ID is not Valid",

        }
        res.send(resBody);
      }
      else {
        ward.update(req.body,
          {
            where: {
              id: req.params.id
            }
          });
        res.send(ward);
      }
    }, function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )

});


// router.get('/details/:id',verify.rou , function (req, res, next){
// // yahan se dr ki details ya patient ki deatils la sukte hain 
// });


router.post('/decreaseinwardbed/:id', verify.rou, function (req, res, next) {
  // ye hai jub ward main ksi ko bed diya to us specific ward ka aik bed kum ho gya 
  // var i;
  db.wardData.findOne({
    where: {
      id: req.params.id
    }
    ,
    attributes: [['id', 'id'], ['noOfBedsEmpty', 'noOfBedsEmpty']]
  })

    .then(
    function (ward) {
      // console.log(ward);
      if (ward == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered ID is not Valid",
        }
      }
      else {
        if (ward.noOfBedsEmpty == 0) {
          res.statusCode = 400;
          var resBody = {
            suucess: false,
            message: " " + "no bed available in this ward for now "
          }
          res.send(resBody);
        }
        else {
          ward.noOfBedsEmpty--;
          ward.save().then(
            function (ward) {
              res.send(ward);
            }
          )

        }
      }
    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    // },
    // function (err) {


    //   res.statusCode = 500;
    //   var resBody = {
    //     error: err.errors,
    //     suucess: false,
    //     message: err.message,
    //   }
    //   res.send(resBody);
    // })

    // .then(function(response){
    //   if(db.wardData.noOfBedsEmpty==0){
    //     res.statusCode = 400;
    //         var resBody = {
    //           suucess: false,
    //           message:err.message+" "+"no bed available in this ward for now "
    //         }
    //         res.send(resBody);
    //   }
    // else{
    //     db.wardData.noOfBedsEmpty=db.wardData.noOfBedsEmpty-1;
    //     res.send("one more bad occupied");
    // }
    // }
    )
});

router.post('/patientremovefromwardbed/:id/:id1', verify.rou, function (req, res, next) {
  // to make wardID attribute null in specific patient table
  // first this will api will run then we will hit is se nechay wali api increase bed wali 
  const wid = req.params.id;
  const pid = req.params.id1;

  db.wardData.findOne({
    where: {
      id: wid
    }
  })
    .then(
    function (ward) {
      console.log(wid);
      console.log(pid);
      if (ward == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of ward is not correct"
        }
        res.send(resBody);
      }
      else {
        db.patientdata.findOne({ where: { id: pid } })
          .then(
          function (patient) {
            if (patient == null) {
              res.statusCode = 404;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "Enterd ID of Patient is not correct"
              }
              res.send(resBody);
            }
            else {
              // console.log(doctor.roomId)
              patient.wardId = null;
              patient.save(); //1111111
              console.log(patient.wardId)
              res.send(patient);
            }
          })
      }
    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )

});


router.post('/increaseinwardbed/:id', verify.rou, function (req, res, next) {
  // ye hai jub ward se ksi ko discharge kiya to us ward k bed main aik add ho gya 
  // var i;
  db.wardData.findOne({
    where: {
      id: req.params.id
    }
    ,
    attributes: [['id', 'id'], ['noOfBedsEmpty', 'noOfBedsEmpty'], ['totalNoOfBeds', 'totalNoOfBeds']]
  })

    .then(
    function (ward) {
      // console.log(ward);
      // if (ward.noOfBedsEmpty == ward.totalNoOfBeds) {
      //   res.statusCode = 400;
      //   var resBody = {
      //     suucess: false,
      //     message: " " + "All beds are already empty "
      //   }
      //   res.send(resBody);
      // }
      // else {
      //   ward.noOfBedsEmpty++;
      //   ward.save().then(
      //     function (ward) {
      //       res.send(ward);
      //     }
      //   )

      // }
      if (ward == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of ward is not correct"
        }
        res.send(resBody);
      }

      else {
        if (ward.noOfBedsEmpty < ward.totalNoOfBeds) {
          ward.noOfBedsEmpty++;
          ward.save().then(
            function (ward) {
              res.send(ward);
            }
          )
        }

        else {
          res.statusCode = 400;
          var resBody = {
            suucess: false,
            message: " " + "All beds are already empty "
          }
          res.send(resBody);
        }
      }

    }
    )
});



router.post('/wardidtodoctor/:id/:id1', verify.rou, function (req, res, next) {
  // ye hai give ward id and and doctor id of doctor whom to assign duty of this ward  
  const wid = req.params.id;
  const did = req.params.id1;

  db.wardData.findOne({
    where: {
      id: wid
    }
    // ,
    // attributes:[['id','id'],['noOfBedsEmpty','noOfBedsEmpty']]
  })

    .then(
    function (ward) {
      console.log(wid);
      //   if(ward.noOfBedsEmpty==0){
      //      res.statusCode = 400;
      //       var resBody = {
      //         suucess: false,
      //         message:" "+"no bed available in this ward for now "
      //       }
      //       res.send(resBody);
      // }
      // else{
      //   ward.noOfBedsEmpty++;
      //   ward.save().then(
      //     function (ward) {
      //       res.send(ward);
      //     }
      //   )
      // }
      // console.log(ward.name);
      console.log(did);
      if (ward == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of ward is not correct"
        }
        res.send(resBody);
      }
      else {
        db.doctordata.findOne({ where: { id: did } })
          .then(
          function (doctor) {
            // console.log(doctor.roomId)
            if (doctor == null) {
              res.statusCode = 404;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "Enterd ID of Doctor is not correct"
              }
              res.send(resBody);
            }
            else {
              doctor.wardId = wid;
              // console.log(doctor);
              doctor.save()
                .then(
                function (doctor) {
                  res.send(doctor);
                }
                )
              // console.log(doctor.wardId)
              // res.send(doctor);
            }
          })
      }
    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )




});


router.post('/wardidtopatient/:id/:id1', function (req, res, next) {
  const wid = req.params.id;
  const pid = req.params.id1;

  db.wardData.findOne({
    where: {
      id: wid
    }
  }).then(
    function (ward) {
      if (!ward) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enterd ID of ward is not correct"
        }
        res.send(resBody);
      }

      if (ward.noOfBedsEmpty != 0) {
        db.patientdata.findOne({ where: { id: pid } })
          .then(
          function (patient) {
            // console.log(doctor.roomId)
            if (!patient) {
              res.statusCode = 404;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "Enterd ID of patient is not correct"
              }
              res.send(resBody);
            }
            else if (patient.wardId != null) {
              res.statusCode = 400;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "This patient is already admitted in other Ward"
              }
              res.send(resBody);
            }
            else if (patient.roomId != null) {
              res.statusCode = 400;
              var resBody = {
                // error: err.errors,
                suucess: false,
                message: "This patient is already admitted in other Room"
              }
              res.send(resBody);
            }
            else {
              patient.wardId = wid;
              patient.save()
              console.log("pagal")
              res.send(patient);
            }
          })
      }
      else {
        res.statusCode = 400;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "All beds are occupied in this bed"
        }
        res.send(resBody);
      }
    })
});

router.get('/getdoctordetail/:id', verify.rou, function (req, res, next) {

  //dr. jo is ward ka hai us ki details ajaen gi
  db.wardData.find({
    where: { id: req.params.id }

  })
    .then(

    function (ward) {
      db.doctordata.findAll({ where: { wardId: ward.id } }).then(function (data) {
        if (data == null) {
          res.statusCode = 404;
          var resBody = {
            // error: err.errors,
            suucess: false,
            message: "There is no data registered against this Id",

          }
          res.send(resBody);
        }
        else {
          res.send(data);
        }
      })

    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    })
  // console.log("abcdefg")

});


router.get('/getpatientdetail/:id', verify.rou, function (req, res, next) {

  //sare patients jo jo is ward ka hai us ki details ajaen gi
  db.wardData.find({
    where: { id: req.params.id }

  })
    .then(

    function (ward) {
      db.patientdata.findAll({ where: { wardId: ward.id } }).then(function (data) {
        if (data == null) {
          res.statusCode = 404;
          var resBody = {
            // error: err.errors,
            suucess: false,
            message: "There is no data registered against this Id",

          }
          res.send(resBody);
        }
        else {
          res.send(data);
        }
      })

    }
    , function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    })
  // console.log("abcdefg")

});

module.exports = router;