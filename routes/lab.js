var express = require('express');
var router = express.Router();
var db = require('../models/index');
var jwt = require('jsonwebtoken');
var ap = require('../app');
var superSecret = 'iloveIAD';
var verify = require('../middleware');
var multer = require('multer');

router.get('/', verify.rou, function (req, res, next) {

  db.labdata.findAll({}).then(
    function (response) {
      res.send(response);
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});

router.get('/byid/:id', verify.rou, function (req, res, next) {

  db.labdata.findOne({
    where: {
      id: req.params.id
    }
  }).then(
    function (response) {
      if (response == null) {
        res.statusCode = 500;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "There is no data registered against this Id",

        }
        res.send(resBody);
      }
      else {
        res.send(response);
      }
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});




router.post('/', verify.rou, function (req, res, next) {
  let lab = {
    patientId: req.body.patientId,
    testResultDate: req.body.testResultDate,
    testName: req.body.testName,
    amountofTest: req.body.amountofTest
  };
  db.patientdata.findOne({
    where: {
      id: lab.patientId
    }
  }).then(function (respon) {
    if (!respon) {
      res.statusCode = 400;
      var resBody = {
        // error: err.errors,
        suucess: false,
        message: "Entered Patient ID is not valid",
      }
      res.send(resBody);
    }
    else {
      db.labdata.create(lab).then(
        function (response) {
          res.send(response);
        },
        function (err) {
          res.statusCode = 500;
          var resBody = {
            error: err.errors,
            suucess: false,
            message: err.message,
          }
          res.send(resBody);
        })
    }
  })



});

router.delete('/del/:id', verify.rou, function (req, res, next) {
  db.labdata.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (response) {
      if (response == 0) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered id is not correct enter correct id",
        }
        res.send(resBody);
      }
      else {
        res.send(String(response));
      }
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});


router.patch('/update/:id', verify.rou, function (req, res, next) {
  const updates = req.body.updates;
  db.labdata.findOne({
    where: {
      id: req.params.id
    }
  })

    .then(lab => {
      return lab.updateAttributes(updates)
    })
    // .then(function (response) {
    //   if (response == null) {
    //     res.statusCode = 404;
    //     var resBody = {
    //       suucess: false,
    //     }
    //     res.send(resBody);
    //   }
    // })
    .then(updatedLab => {
      if (updatedLab == null) {
        // console.log("idher aya")
        res.statusCode = 404;
        var resBody = {
          suucess: false,
        }
        res.send(resBody);
      }
      else
        res.send(updatedLab);
    },
    function (err) {

      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message + " " + "maybe you're entering unvalid Lab ID ",
      }
      res.send(resBody);
    }
    );

});

router.put('/updaterecord/:id', verify.rou, function (req, res, next) {

  // db.doctor.findOne({          //pagal kya bhnd mara hai lanat hai aap pr :/
  //   where: {
  //     id: req.params.id
  //   }
  // })
  db.labdata.findOne({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (lab) {
      if (lab == null) {
        res.statusCode = 500;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered ID is not Valid",

        }
        res.send(resBody);
      }
      else {
        lab.update(req.body,
          {
            where: {
              id: req.params.id
            }
          });
        res.send(lab);
      }
    }, function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )

});

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/LabReports/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
  }
})

var upload = multer({ storage: storage }).single('labreport')
// var upload = multer({ storage: storage }).array('labreport', [10])

router.post('/labreportupload/:id', verify.rou, function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      // An error occurred when uploading
      // return
      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    }
    else {
      // console.log(req.file.path);
      db.labdata.findOne({
        where: {
          id: req.params.id
        }
      })
        // .then(
        // function(lab){
        //     res.send(lab);
        // })
        .then(
        function (lab) {
          // lab.update(req.body,
          // console.log(lab.filepath + "pehle");
          if (lab == null) {
            res.statusCode = 404;
            var resBody = {
              // error: err.errors,
              suucess: false,
              message: "Entered ID is not Valid",

            }
            res.send(resBody);
          }
          else {
            lab.update(lab.filepath = req.file.path,
              {
                where: {
                  id: req.params.id
                }
              });
            lab.save();
            res.send(lab);
            console.log(lab.filepath + "baad main");
          }
        }
        , function (err) {

          res.statusCode = 500;
          var resBody = {
            error: err.errors,
            suucess: false,
            message: err.message,
          }
          res.send(resBody);
        })
      // .then(
      // function(lab){
      //     res.send(lab);
      // })

      // res.send("file upload hogyi be");
    }

  })
});


router.get('/getfile/:id', verify.rou, function (req, res, next) {
  // to get back path of file which we uploaded
  db.labdata.findOne({
    where: {
      id: req.params.id
    },
    attributes: [['filepath', 'file']]
  }).
    then(
    function (response) {
      if (response == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered id is not Valid",
        }
        res.send(resBody);
      }

      res.send(response);
      // console.log(response);
      // res.json({ error_code: 0, err_desc: null , message:"abcd" });
    },
    function (err) {

      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});


module.exports = router;