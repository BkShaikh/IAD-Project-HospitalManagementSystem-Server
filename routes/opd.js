var express = require('express');
var router = express.Router();
var db = require('../models/index');
var verify = require('../middleware');

router.get('/', verify.rou, function (req, res, next) {

  db.opdData.findAll({}).then(
    function (response) {
      res.send(response);
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});


router.get('/detailbyday/:dayofOpds', verify.rou, function (req, res, next) {
  var day = req.params.dayofOpds;
  // console.log(req.params.dayofOpds);
  // console.log(day);
  db.opdData.findOne({
    where: { dayofOpds: day }

  }).then(
    function (response) {
      if (!response) {
        res.statusCode = 400;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "check maybe you're entering wrong spelling of day OR there is no OPD on this day",
        }
        res.send(resBody);
      }
      else {
        res.send(response);
      }
    },
    function (err) {
      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});


router.post('/', verify.rou, function (req, res, next) {

  let opde = {
    timingofODs: req.body.timingofODs,
    dayofOpds: req.body.dayofOpds,
    doctorId: req.body.doctorId,
    // assignedToDr: req.body.assignedToDr,
    roomId: req.body.roomId
  }
  db.doctordata.findOne({
    where: {
      id: opde.doctorId
    }
  })
    .then(function (doctor) {
      if (!doctor) {
        res.statusCode = 400;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered Doctor ID is invalid. Enter valid doctor ID",
        }
        res.send(resBody);
      }
      else {
        console.log(doctor.firstname);
        db.opdData.create(opde)
          .then(
          function (response) {
            response.assignedToDr = doctor.firstname
            response.save()
            res.send(response);
          },
          function (err) {
            res.statusCode = 500;
            var resBody = {
              error: err.errors,
              suucess: false,
              message: err.message,
            }
            res.send(resBody);
          })
      }
    })
  // console.log(req.body);
  // db.opdData.create(opde).then(
  //   function (response) {
  //     res.send(response);
  //   },
  //   function (err) {


  //     res.statusCode = 500;
  //     var resBody = {
  //       error: err.errors,
  //       suucess: false,
  //       message: err.message,
  //     }
  //     res.send(resBody);
  //   })


});

router.post('/opdtimingtodoctor/:id/:id1', verify.rou, function (req, res, next) {
  oid = req.params.id;
  did = req.params.id1;
  db.opdData.findOne({
    where: {
      id: oid
    }
  }).then(function (opd) {
    if (!opd) {
      res.statusCode = 404;
      var resBody = {
        // error: err.errors,
        suucess: false,
        message: "Enterd ID of OPD is not Valid",
      }
      res.send(resBody);
    }
    else {
      db.doctordata.findOne({
        where: {
          id: did
        }
      }).then(function (doctor) {
        if (!doctor) {
          res.statusCode = 404;
          var resBody = {
            // error: err.errors,
            suucess: false,
            message: "Enterd ID of Doctor is not Valid",
          }
          res.send(resBody);
        }
        else {
          // console.log(opd.timingofODs);
          // console.log(doctor.opdtiming);
          if (doctor.opdtiming == null) {
            doctor.opdtiming = opd.timingofODs;
            doctor.save()
            res.send(doctor);
          }
          else {
            res.statusCode = 400;
            var resBody = {
              // error: err.errors,
              suucess: false,
              message: "This Doctor has already OPD timings",
            }
            res.send(resBody);
          }
        }
      })
    }
  }, function (err) {
    res.statusCode = 500;
    var resBody = {
      error: err.errors,
      suucess: false,
      message: err.message,
    }
    res.send(resBody);
  })
});

router.delete('/del/:id', verify.rou, function (req, res, next) {

  db.opdData.findOne({
    where: {
      id: req.params.id
    }
  }).then(function (opd) {
    db.doctordata.findOne({
      where: {
        id: opd.doctorId
      }
    }).then(function (doctor) {
      doctor.opdtiming = null;
      doctor.save()
    })
  })

  db.opdData.destroy({
    where: {
      id: req.params.id
    }
  })
    // .then(deletedPatient=> {
    //     res.json(deletedPatient);
    //     // res.send(response);

    // });

    // })
    .then(
    function (response) {
      if (response == 0) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered id is not correct enter correct id",
        }
        res.send(resBody);
      }
      else {
        res.send(String(response));
      }
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});



router.get('/getopddetail/:id', verify.rou, function (req, res, next) {

  // db.opdData.findAll({
  //   include: [{ model : db.patientdata }]

  // })
  //   .then(
  //   function (response) {
  //     res.send(response);
  //   },
  db.opdData.find({
    where: { id: req.params.id }

  })
    .then(

    function (opd) {
      if (!opd) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Enter ID is not valid",
        }
        res.send(resBody);
      }
      else {
        res.send(opd)
      }
    }
    , function (err) {
      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
  // console.log("abcdefg")


});





router.patch('/update/:id', verify.rou, function (req, res, next) {
  const updates = req.body.updates;
  db.opdData.findOne({
    where: {
      id: req.params.id
    }
  })

    .then(opde => {
      return opde.updateAttributes(updates)
    })
    // .then(function (response) {
    //   if (response == null) {
    //     res.statusCode = 404;
    //     var resBody = {
    //       suucess: false,
    //     }
    //     res.send(resBody);
    //   }
    // })
    .then(updatedOpde => {
      if (updatedOpde == null) {
        console.log("idher aya")
        res.statusCode = 404;
        var resBody = {
          suucess: false,
        }
        res.send(resBody);
      }
      else
        res.send(updatedOpde);
    },
    function (err) {

      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message + " " + "maybe you're entering unvalid OPD ID ",
      }
      res.send(resBody);
    }
    );

});

router.put('/updaterecord/:id', verify.rou, function (req, res, next) {

  // db.doctor.findOne({          //pagal kya bhnd mara hai lanat hai aap pr :/
  //   where: {
  //     id: req.params.id
  //   }
  // })
  db.opdData.findOne({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (opd) {
      if (opd == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered ID is not Valid",
        }
        res.send(resBody);
      }
      else {
        console.log(opd.doctorId + " " + " ye shi ani chaheye pehel wali");
        db.doctordata.findOne({
          where: {
            id: opd.doctorId
          }
        }).then(function (doctor) {
          if (doctor != null && doctor.opdtiming != null) {
            doctor.opdtiming = null;
            doctor.save()
          }
          // res.send(doctor);
        })
        opd.update(req.body,
          {
            where: {
              id: req.params.id
            }
          });
        console.log(opd.timingofODs);
        db.doctordata.findOne({
          where: {
            id: opd.doctorId
          }
        }).then(function (doctor) {
          if (!doctor) {
            console.log(opd.doctorId);
            opd.doctorId = null;
            opd.save();
            console.log(opd.doctorId);
            var resBody = {
              suucess: false,
              message: "Can't assign this to doctorId as entered doctorId is invalid so it is set to null now"
            }
            res.send(resBody);
          }
          else {
            opd.assignedToDr = doctor.firstname;
            doctor.opdtiming = opd.timingofODs;
            opd.save();
            doctor.save();
            res.send(opd);
          }
        })
      }
    }, function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )

});


router.get('/opdtimingtodoctor/:id', verify.rou, function (req, res, next) {
  //this one to call before put api and delete api of opd
  oid = req.params.id;
  db.opdData.findOne({
    where: {
      id: req.params.id
    }
  }).then(function (opd) {
    db.doctordata.findOne({
      where: {
        id: opd.doctorId
      }
    }).then(function (doctor) {
      doctor.opdtiming = null;
      doctor.save()
      res.send(doctor);
    })
  })
});

router.get('/removeopdidfrompatient/:id', verify.rou, function (req, res, next) {
  //to run before delete api
  db.patientdata.update({
    opdId: null
  }, {
      where: {
        opdId: req.params.id
      }
    }).then(
    function (response) {
      res.send(response);
    })
});

router.post('/opdidtopatient/:id/:id1', verify.rou, function (req, res, next) {
  oid = req.params.id;
  pid = req.params.id1;

  db.opdData.findOne({
    where: {
      id: oid
    }
  }).then(function (opd) {
    if (!opd) {
      res.statusCode = 404;
      var resBody = {
        // error: err.errors,
        suucess: false,
        message: "Enterd ID of OPD is not Valid",
      }
      res.send(resBody);
    }
    else {
      db.patientdata.findOne({
        where: {
          id: pid
        }
      }).then(function (patient) {
        if (!patient) {
          res.statusCode = 404;
          var resBody = {
            // error: err.errors,
            suucess: false,
            message: "Enterd ID of Patient is not Valid",
          }
        }
        else if (patient.opdId == null) {
          patient.opdId = oid
          patient.save()
          res.send(patient);
        }
        else {
          res.statusCode = 400;
          var resBody = {
            // error: err.errors,
            suucess: false,
            message: "This Patient has already a OPD Id",
          }
          res.send(resBody);
        }
      })
    }
  }, function (err) {
    res.statusCode = 500;
    var resBody = {
      error: err.errors,
      suucess: false,
      message: err.message,
    }
    res.send(resBody);
  })
});



router.get('/a/:id', function (req, res, next) {

  db.patientdata.update({
    opdId: null
  }, {
      where: {
        opdId: req.params.id
      }
    }).then(
    function (response) {
      res.send(response);
    })



  //   let a, b
  //   var g=1;

  //   db.patientdata.count({
  //     where: {
  //       opdId: req.params.id
  //     }
  //   }).then(c => {
  //     a = c;
  //     b = c + 1;
  //     console.log(c + " " + "aaaa" + a + "bbbbb" + b)
  //   }).then(function(re){
  //  for (var index = 0; index < a; index++) {
  //     console.log("kuch"+" "+ g)
  //     g++

  //     if (a > 0) {
  //       db.patientdata.findOne({
  //         where: {
  //           opdId: req.params.id
  //         }
  //       }).then(function (patient) {
  //         if (patient == null) {
  //           n = 0;
  //           console.log("aik bar aya "+" "+ g)
  //           g++
  //           res.send("a")
  //              patient.save();
  //         }
  //         else {
  //           console.log(b)
  //           console.log(b + " " + "yahab aya"+" "+ g)
  //           g++
  //           patient.opdId = null
  //           // res.send("sa")
  //         }

  //       })
  //     }
  //     a--;
  //  }   
  //   })



  // console.log(a)
  // 

  // res.send("A")
})









module.exports = router;