var express = require('express');
var router = express.Router();
var db = require('../models/index');
var jwt = require('jsonwebtoken');
var ap = require('../app');
var superSecret = 'iloveIAD';
var verify = require('../middleware');

router.get('/', verify.rou, function (req, res, next) {

  db.medicalhistorydata.findAll({}).then(
    function (response) {
      res.send(response);
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});

router.get('/byid/:id', verify.rou, function (req, res, next) {

  db.medicalhistorydata.findOne({
    where: {
      id: req.params.id
    }
  }).then(
    function (response) {
      if (!response) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Invaid ID data not found",
        }
        res.send(resBody);
      }
      else {
        res.send(response);
      }
    },
    function (err) {
      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});




router.post('/', verify.rou, function (req, res, next) {
  let mh = {
    patientId: req.body.patientId,
    bloodGroup: req.body.bloodGroup,
    lastDateOfCheckup: req.body.lastDateOfCheckup,
    lastDateOfDischarge: req.body.lastDateOfDischarge,
    familyDiseases: req.body.familyDiseases,
    habbits: req.body.habbits,
    prescribedMedicines: req.body.prescribedMedicines,
    mainDiseases: req.body.mainDiseases,
    otherPotentialDiseases: req.body.otherPotentialDiseases,
    lastDateofAdmit: req.body.lastDateofAdmit
  };
  db.patientdata.findOne({
    where: {
      id: mh.patientId
    }
  }).then(function (respon) {
    if (!respon) {
      res.statusCode = 400;
      var resBody = {
        // error: err.errors,
        suucess: false,
        message: "Entered Patient ID is not valid",
      }
      res.send(resBody);
    }
    else {
      db.medicalhistorydata.create(mh).then(
        function (response) {
          res.send(response);
        },
        function (err) {
          res.statusCode = 500;
          var resBody = {
            error: err.errors,
            suucess: false,
            message: err.message,
          }
          res.send(resBody);
        })
    }
  })
  // console.log(req.body);
});

router.delete('/del/:id', verify.rou, function (req, res, next) {
  db.medicalhistorydata.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (response) {
      if (response == 0) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered id is not correct enter correct id",
        }
        res.send(resBody);
      }
      else {
        res.send(String(response));
      }
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});


router.patch('/update/:id', verify.rou, function (req, res, next) {
  const updates = req.body.updates;
  db.medicalhistorydata.findOne({
    where: {
      id: req.params.id
    }
  })

    .then(mh => {
      return mh.updateAttributes(updates)
    })
    // .then(function (response) {
    //   if (response == null) {
    //     res.statusCode = 404;
    //     var resBody = {
    //       suucess: false,
    //     }
    //     res.send(resBody);
    //   }
    // })
    .then(updatedMh => {
      if (updatedMh == null) {
        // console.log("idher aya")
        res.statusCode = 404;
        var resBody = {
          suucess: false,
        }
        res.send(resBody);
      }
      else
        res.send(updatedMh);
    },
    function (err) {

      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message + " " + "maybe you're entering unvalid Medical History ID ",
      }
      res.send(resBody);
    }
    );

});

router.put('/updaterecord/:id', verify.rou, function (req, res, next) {

  // db.doctor.findOne({          //pagal kya bhnd mara hai lanat hai aap pr :/
  //   where: {
  //     id: req.params.id
  //   }
  // })
  db.medicalhistorydata.findOne({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (mh) {
      if (mh == null) {
        res.statusCode = 500;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered ID is not Valid",

        }
        res.send(resBody);
      }
      else {
        mh.update(req.body,
          {
            where: {
              id: req.params.id
            }
          });
        res.send(mh);
      }
    }, function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,

      }
      res.send(resBody);
    }
    )

});

module.exports = router;