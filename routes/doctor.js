var express = require('express');
var router = express.Router();
var db = require('../models/index');
var verify = require('../middleware');
/* GET users listing. */
router.get('/', verify.rou, function (req, res, next) {

  db.doctordata.findAll({}).then(
    function (response) {
      res.send(response);
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});


router.get('/byid/:id', verify.rou, function (req, res, next) {

  db.doctordata.findOne({
    where: {
      id: req.params.id
    }
  }).then(
    function (response) {
      if (response == null) {
        // console.log("idher che"  );
        // function (err) {


        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          // message: err.message,
        }
        res.send(resBody);
        // }
        // res.send("entered ID is not been set yet");
      }
      else {
        // console.log("nhi idher che", response);
        res.send(response);

      }
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});



router.get('/byname/:name', verify.rou, function (req, res, next) {
  var name = req.params.name;
  db.doctordata.findOne({
    where: {
      firstname: name
    }
  }).then(
    function (response) {
      if (response == null) {
        // console.log("idher che"  );
        // function (err) {


        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          // message: err.message,
        }
        res.send(resBody);
        // }
        // res.send("entered ID is not been set yet");
      }
      else {
        // console.log("nhi idher che", response);
        res.send(response);

      }
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});



router.post('/', verify.rou, function (req, res, next) {
  let doctor = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email,
    address: req.body.address,
    city: req.body.city,
    cnicno: req.body.cnicno,
    department: req.body.department,
    specialization: req.body.specialization,
    emergencyschedule: req.body.emergencyschedule,
    opdtiming: req.body.opdtiming,
    post: req.body.post,
    wardId: req.body.wardId,
    roomId: req.body.roomId
  };
  console.log(req.body);
  db.doctordata.create(doctor).then(
    function (response) {
      res.send(response);
    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});

router.delete('/del/:id', verify.rou, function (req, res, next) {
  did = req.params.id

  // db.opdData.findOne({
  //   where: {
  //     doctorId: req.params.id
  //   }
  // }).then(function (opd) {
  //   if (!opd) {
  //     message: "there is no OPD of this doctor"
  //   }
  //   else {
  //     opd.doctorId = null
  //     opd.assignedToDr = null
  //     opd.save();
  //   }
  // })
  db.opdData.update({
    doctorId: null
  }, {
      where: {
        doctorId: req.params.id
      }
    })

  db.patientdata.update({
    consultantdoc: null
  }, {
      where: {
        consultantdoc: req.params.id
      }
    })

  db.doctordata.destroy({
    where: {
      id: req.params.id
    }
  })
    // .then(deletedPatient=> {
    //     res.json(deletedPatient);
    //     // res.send(response);

    // });

    // })
    .then(
    function (response) {
      if (response == 0) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered id is not correct enter correct id",
        }
        res.send(resBody);
      }
      else {
        res.send(String(response));
      }
    },
    function (err) {
      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});


router.patch('/update/:id', verify.rou, function (req, res, next) {
  const updates = req.body.updates;
  db.doctordata.findOne({
    where: {
      id: req.params.id
    }
  })

    .then(doctor => {
      return doctor.updateAttributes(updates)
    })
    // .then(function (response) {
    //   if (response == null) {
    //     res.statusCode = 404;
    //     var resBody = {
    //       suucess: false,
    //     }
    //     res.send(resBody);
    //   }
    // })
    .then(updatedDoctor => {
      if (updatedDoctor == null) {
        // console.log("idher aya")
        res.statusCode = 404;
        var resBody = {
          suucess: false,
        }
        res.send(resBody);
      }
      else
        res.send(updatedDoctor);
    },
    function (err) {

      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    }
    );

});

router.put('/updateput/:id', verify.rou, function (req, res, next) {

  var id1 = req.params.id;
  var updates = req.body;

  db.doctordata.update(req.body,
    {
      where: {
        id: id1
      }
    },
    function (err, numberAffected) {
      if (err) return console.log(err);
      console.log('Updated %d musicians', numberAffected);
      return res.send(202);
    });


});


router.put('/updaterecord/:id', verify.rou, function (req, res, next) {

  // db.doctor.findOne({          //pagal kya bhnd mara hai lanat hai aap pr :/
  //   where: {
  //     id: req.params.id
  //   }
  // })
  db.doctordata.findOne({
    where: {
      id: req.params.id
    }
  })
    .then(
    function (doc) {
      if (doc == null) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "Entered ID is not Valid",

        }
        res.send(resBody);
      }
      else {
        doc.update(req.body,
          {
            where: {
              id: req.params.id
            }
          });
        res.send(doc);
      }
    }, function (err) {
      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
});


router.get('/getroomdetail/:name', verify.rou, function (req, res, next) {

  var name = req.params.name;
  db.doctordata.findOne({
    where: {
      firstname: name
    }
  })
    .then(
    function (doc) {
      if (!doc) {
        res.statusCode = 400;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "No such Dr registered",
        }
        res.send(resBody);
      }
      else {
        db.roomData.findOne({ where: { assignedTo: name } }).then(function (data) {
          res.send(data);
          console.log(name)
        })
      }
    }
    , function (err) {
      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })
  // console.log("abcdefg")
  console.log(name);
});


router.get('/getopddetails/:id',verify.rou, function (req, res, next) {

  db.doctordata.find({
    include: [{
      model: db.opdData,
      where: {
        doctorId: req.params.id
      }
    }]
  })
    .then(
    function (response) {
      if (!response) {
        res.statusCode = 404;
        var resBody = {
          // error: err.errors,
          suucess: false,
          message: "There is no Such doctor ID Enter Valid ID",
        }
        res.send(resBody);
      }
      // console.log(response.opdDatum);
      else {
        res.send(response.opdDatum);
      }

    },
    function (err) {


      res.statusCode = 500;
      var resBody = {
        error: err.errors,
        suucess: false,
        message: err.message,
      }
      res.send(resBody);
    })


});


module.exports = router;
