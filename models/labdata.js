'use strict';
module.exports = function (sequelize, DataTypes) {
  var labdata = sequelize.define('labdata', {
    patientId: DataTypes.INTEGER,
    testResultDate: DataTypes.DATEONLY,
    testName: DataTypes.STRING,
    amountofTest: DataTypes.INTEGER,
    filepath: DataTypes.STRING
  }, {
      classMethods: {
        associate: function (models) {
          // associations can be defined here
          labdata.belongsTo(models.patientdata,
            { foreignKey: 'patientId', as: "patientdata" }
          )
        }
      }
    });
  return labdata;
};