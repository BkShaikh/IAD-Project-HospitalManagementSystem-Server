'use strict';
module.exports = function(sequelize, DataTypes) {
  var opdbooking = sequelize.define('opdbooking', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    cnicno: DataTypes.STRING,
    contactno: DataTypes.STRING,
    bookingId:DataTypes.INTEGER,
    opdId: DataTypes.INTEGER,
    time: DataTypes.TIME,
    date:DataTypes.DATEONLY
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return opdbooking;
};