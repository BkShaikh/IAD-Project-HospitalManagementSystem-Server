'use strict';
module.exports = function(sequelize, DataTypes) {
  var opdconsultancy = sequelize.define('opdconsultancy', {
    date: DataTypes.DATEONLY,
    bdlevel: DataTypes.STRING,
    fever: DataTypes.STRING,
    prescribeddrugs: DataTypes.STRING,
    patientId: DataTypes.INTEGER,
    patientcnic: DataTypes.STRING,
    comments: DataTypes.STRING,
    potentialdisease: DataTypes.STRING,
    otherproblem: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return opdconsultancy;
};